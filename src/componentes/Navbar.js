import React from 'react';
import { Link, NavLink, useNavigate } from 'react-router-dom';

const Navbar = () => {

const navigate = useNavigate();

const cerrarSesion = () => {
  localStorage.removeItem("token");
  navigate("/");

}

  return (

 
      <nav className="main-header navbar navbar-expand navbar navbar-dark bg-dark">
        <ul className="navbar-nav">
          <li className="nav-item">
            <Link to={"#"} className="nav-link" data-widget="pushmenu" role="button"><i className="fas fa-bars" /></Link>
          </li>
          <li className="nav-item d-none d-sm-inline-block">
            <NavLink to={"/home"} exact="true" className="nav-link" end>Inicio</NavLink>
          </li>
          <li className="nav-item d-none d-sm-inline-block">
            <NavLink to={"/productos-user"} className="nav-link" end>Productos</NavLink>
          </li>
          <li className="nav-item d-none d-sm-inline-block">
            <Link to={"/carrito"} className="btn btn-block btn-warning"><b>Carrito</b></Link>
          </li>

        </ul>
        <ul className="navbar-nav ml-auto">

          <li className="nav-item d-none d-sm-inline-block">
            <Link to={"/productos-admin"} className="btn btn-block btn-success">Admin</Link>
          </li>

          <li className="nav-item">
            <Link to={"#"} className="nav-link" data-widget="fullscreen" role="button">
              <i className="fas fa-expand-arrows-alt" />
            </Link>
          </li>


          <li className="nav-item d-none d-sm-inline-block">
                    <strong onClick={cerrarSesion} className="nav-link" style={{cursor: 'pointer'}}>Salir</strong>
                </li>

        </ul>
      </nav>
  );
}

export default Navbar;