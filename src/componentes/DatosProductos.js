import React from "react";
import { Link } from "react-router-dom";



const DatosProductos = () => {

  return (
    <section className="content">
      <div className="card">
        <div className="col-sm-12 col-md-6">
          <div id="example1_filter" className="dataTables_filter">
            <label>
              Buscar:
              <input
                type="search"
                className="form-control form-control-sm"
                placeholder
                aria-controls="example1"
              />
            </label>
          </div>
        </div>
        <div className="card-body">
          <div
            id="example1_wrapper"
            className="dataTables_wrapper dt-bootstrap4"
          >
            <div className="row">
              <div className="col-sm-12">
                <table
                  id="example1"
                  className="table table-bordered table-striped dataTable dtr-inline"
                  aria-describedby="example1_info"
                >
                  <thead>
                    <tr>
                      <th
                      style={{width: '10%'}}
                        className="sorting sorting_asc"
                        tabIndex={0}
                        aria-controls="example1"
                        rowSpan={1}
                        colSpan={1}
                        aria-sort="ascending"
                        aria-label="Rendering engine: activate to sort column descending"
                      >
                        Id
                      </th>

                      <th
                      style={{width: '35%'}}
                        className="sorting sorting_asc"
                        tabIndex={0}
                        aria-controls="example1"
                        rowSpan={1}
                        colSpan={1}
                        aria-sort="ascending"
                        aria-label="Rendering engine: activate to sort column descending"
                      >
                        Nombre
                      </th>

                      <th
                      style={{width: '20%'}}
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example1"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Browser: activate to sort column ascending"
                      >
                        Mascota
                      </th>

                      <th
                      style={{width: '20%'}}
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example1"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Platform(s): activate to sort column ascending"
                      >
                        Categoría
                      </th>

                      <th
                      style={{width: '15%'}}
                        className="sorting"
                        tabIndex={0}
                        aria-controls="example1"
                        rowSpan={1}
                        colSpan={1}
                        aria-label="Engine version: activate to sort column ascending"
                      >
                        Precio
                      </th>
                    </tr>
                  </thead>
                  <tbody>

                    <tr className="odd">
                      <th>&nbsp;</th>
                      <td>&nbsp;</td> 
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td> 
                    </tr>

                  </tbody>

                  <tfoot>
                    <tr>
                      <th rowSpan={1} colSpan={1}>
                        Id
                      </th>
                      <th rowSpan={1} colSpan={1}>
                        Nombre
                      </th>
                      <th rowSpan={1} colSpan={1}>
                        Mascota
                      </th>
                      <th rowSpan={1} colSpan={1}>
                        Categoría
                      </th>
                      <th rowSpan={1} colSpan={1}>
                        Precio
                      </th>
                    </tr>
                  </tfoot>

                </table>
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 col-md-5">
                <div
                  className="dataTables_info"
                  id="example1_info"
                  role="status"
                  aria-live="polite"
                >
                  Showing 1 to 10 of 57 entries
                </div>
              </div>
              <div className="col-sm-12 col-md-7">
                <div
                  className="dataTables_paginate paging_simple_numbers"
                  id="example1_paginate"
                >
                  <ul className="pagination">
                    <li
                      className="paginate_button page-item previous disabled"
                      id="example1_previous"
                    >
                      <Link
                        to={"#"}
                        aria-controls="example1"
                        data-dt-idx={0}
                        tabIndex={0}
                        className="page-link"
                      >
                        Anterior
                      </Link>
                    </li>
                    <li className="paginate_button page-item active">
                      <Link
                        to={"#"}
                        aria-controls="example1"
                        data-dt-idx={1}
                        tabIndex={0}
                        className="page-link"
                      >
                        1
                      </Link>
                    </li>
                    <li className="paginate_button page-item ">
                      <Link
                        to={"#"}
                        aria-controls="example1"
                        data-dt-idx={2}
                        tabIndex={0}
                        className="page-link"
                      >
                        2
                      </Link>
                    </li>
                    <li className="paginate_button page-item ">
                      <Link
                        to={"#"}
                        aria-controls="example1"
                        data-dt-idx={3}
                        tabIndex={0}
                        className="page-link"
                      >
                        3
                      </Link>
                    </li>
                    <li className="paginate_button page-item ">
                      <Link
                        to={"#"}
                        aria-controls="example1"
                        data-dt-idx={4}
                        tabIndex={0}
                        className="page-link"
                      >
                        4
                      </Link>
                    </li>
                    <li className="paginate_button page-item ">
                      <Link
                        to={"#"}
                        aria-controls="example1"
                        data-dt-idx={5}
                        tabIndex={0}
                        className="page-link"
                      >
                        5
                      </Link>
                    </li>
                    <li className="paginate_button page-item ">
                      <Link
                        to={"#"}
                        aria-controls="example1"
                        data-dt-idx={6}
                        tabIndex={0}
                        className="page-link"
                      >
                        6
                      </Link>
                    </li>
                    <li
                      className="paginate_button page-item next"
                      id="example1_next"
                    >
                      <Link
                        to={"#"}
                        aria-controls="example1"
                        data-dt-idx={7}
                        tabIndex={0}
                        className="page-link"
                      >
                        Siguiente
                      </Link>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default DatosProductos;
