import React from 'react';
//import { Link } from 'react-router-dom';

const Overlay = () => {
   return (

<div className="card card-success">
  <div className="card-body">
    <div className="row">
      <div className="col-md-12 col-lg-6 col-xl-4">
        <div className="card mb-2 bg-gradient-dark">
          <img className="card-img-top" src="https://imageio.forbes.com/specials-images/dam/imageserve/1068867780/960x0.jpg?format=jpg&width=960" alt=""/>
          <div className="card-img-overlay d-flex flex-column justify-content-end">
            <h5 className="card-title text-primary text-white">Card Title</h5>
            <p className="card-text text-white pb-2 pt-1">Lorem ipsum dolor sit amet, consectetur adipisicing elit sed do eiusmod tempor.</p>

          </div>
        </div>
      </div>
      <div className="col-md-12 col-lg-6 col-xl-4">
        <div className="card mb-2">
          <img className="card-img-top" src="https://images2.minutemediacdn.com/image/upload/c_fill,w_1440,ar_16:9,f_auto,q_auto,g_auto/shape/cover/sport/78996-istock-682216682-a84e814e7edb9e8457cb94cf0d8cd613.jpg" alt="" />
          <div className="card-img-overlay d-flex flex-column justify-content-center">
            <h5 className="card-title text-white mt-5 pt-2">Card Title</h5>
            <p className="card-text pb-2 pt-1 text-white">
              Lorem ipsum dolor sit amet, <br />
              consectetur adipisicing elit <br />
              sed do eiusmod tempor.
            </p>

          </div>
        </div>
      </div>
      <div className="col-md-12 col-lg-6 col-xl-4">
        <div className="card mb-2">
          <img className="card-img-top" src="https://www.zooplus.es/magazine/wp-content/uploads/2019/04/hamster-rasseportrait.jpg" alt=""/>
          <div className="card-img-overlay">
            <h5 className="card-title text-primary">Card Title</h5>
            <p className="card-text pb-1 pt-1 text-white">
              Lorem ipsum dolor <br />
              sit amet, consectetur <br />
              adipisicing elit sed <br />
              do eiusmod tempor. </p>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>


               );
}

               export default Overlay;