import React from 'react';
import { Link, NavLink } from 'react-router-dom';

const Menu = () => {
   return (
      <nav className="mt-2">
         <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            {/*Categoria Mascotas*/}
            <li className="nav-item">
               <Link to={"#"} className="nav-link">
                  <i className="fa fa-dog" aria-hidden="true"> </i> <p></p>
                  <p>
                     Categoría de Mascotas
                     <i className="right fas fa-angle-left" />
                  </p>
               </Link>
               <ul className="nav nav-treeview">
                  <li className="nav-item">
                     <a href="../../index.html" className="nav-link">
                        <i className="far fa-circle nav-icon" />
                        <p>Perros</p>
                     </a>
                  </li>
                  <li className="nav-item">
                     <a href="../../index2.html" className="nav-link">
                        <i className="far fa-circle nav-icon" />
                        <p>Gatos</p>
                     </a>
                  </li>
                  <li className="nav-item">
                     <a href="../../index3.html" className="nav-link">
                        <i className="far fa-circle nav-icon" />
                        <p>Aves</p>
                     </a>
                  </li>
                  <li className="nav-item">
                     <a href="../../index3.html" className="nav-link">
                        <i className="far fa-circle nav-icon" />
                        <p>Hamsters</p>
                     </a>
                  </li>
               </ul>
            </li>
            {/*Categoria Productos*/}
            <li className="nav-item">
               <Link to={"#"} className="nav-link">
                  <i className="fa fa-store" aria-hidden="true"> </i> <p></p>
                  <p>
                     Categoría de Productos
                     <i className="right fas fa-angle-left" />
                  </p>
               </Link>
               <ul className="nav nav-treeview">
                  <li className="nav-item">
                     <a href="../../index.html" className="nav-link">
                        <i className="far fa-circle nav-icon" />
                        <p>Comida</p>
                     </a>
                  </li>
                  <li className="nav-item">
                     <a href="../../index2.html" className="nav-link">
                        <i className="far fa-circle nav-icon" />
                        <p>Aseo</p>
                     </a>
                  </li>
                  <li className="nav-item">
                     <a href="../../index3.html" className="nav-link">
                        <i className="far fa-circle nav-icon" />
                        <p>Juguetes</p>
                     </a>
                  </li>
                  <li className="nav-item">
                     <a href="../../index3.html" className="nav-link">
                        <i className="far fa-circle nav-icon" />
                        <p>Accesorios</p>
                     </a>
                  </li>
               </ul>
            </li>

            {/*Contactenos*/}
            <li className="nav-item">
               <NavLink to={"/contactenos"} className="nav-link" end>
                  <i className="fa fa-envelope" aria-hidden="true"> </i> <p></p>
                  <p>
                     Contáctenos
                     {/*<i className="right fas fa-angle-left" />*/}
                  </p>
               </NavLink>
            </li>
         </ul>
      </nav>
   );
}

export default Menu;