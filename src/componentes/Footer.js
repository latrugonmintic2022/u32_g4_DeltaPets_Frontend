import React from 'react';
import {Link} from 'react-router-dom';

const Footer = () => {
   return ( 

<footer className="main-footer">
  <div className="float-right d-none d-sm-block">
    <b>Version</b> 1.1.0
  </div>
  <strong>Copyright © 2022 <Link to={"/home"}>Deltapets.com</Link>.</strong> All rights reserved.
</footer>


    );
}
 
export default Footer;