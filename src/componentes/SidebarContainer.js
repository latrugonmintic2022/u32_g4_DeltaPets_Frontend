import React from 'react';
import Menu from './Menu';
import Logo from './deltapets.png';
import { Link } from 'react-router-dom';

const SidebarContainer = () => {
   return (


      < aside className="main-sidebar sidebar-dark-primary elevation-4" >
         {/* Brand Logo */}
         <Link to={"/home"} className="brand-link" >
            <img src={Logo} //"../../dist/img/AdminLTELogo.png" 
               alt="AdminLTE Logo" className="brand-image img-circle elevation-3" 
               style={{ opacity: '.9' }} />
            <span className="brand-text font-weight-light">DeltaPets.com</span>
         </Link >
         {/* Sidebar */}
         < div className="sidebar" >
            {/* Sidebar user (optional) */}
            < div className="user-panel mt-3 pb-3 mb-3 d-flex" >
               <div className="info"> &nbsp; </div>
               <div className="info"> &nbsp; </div>
               <div className="info"> &nbsp; </div>
               {/*<div className="image">
        <img src="../../dist/img/user2-160x160.jpg" 
        className="img-circle elevation-2" 
        alt="User Image" />
   </div>*/}
               <div className="info">
                  <Link to={"/home"} className="d-block">Menú Principal</Link>
               </div>
            </div >
            {/* SidebarSearch Form 
   < div className = "form-inline" >
      <div className="input-group" data-widget="sidebar-search">
         <input className="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search" />
         <div className="input-group-append">
            <button className="btn btn-sidebar">
               <i className="fas fa-search fa-fw" />
            </button>
         </div>
      </div>
    </div >
   */ }
            <Menu> </Menu>
         </div >
      </aside >
   );
}

export default SidebarContainer;