import React from 'react';
import ContentHeader from '../../componentes/ContentHeader';
import SidebarContainer from '../../componentes/SidebarContainer';
import Navbar from '../../componentes/Navbar';
import Footer from '../../componentes/Footer';


const Contactenos = () => {
   return (
      <div className="wrapper">
         <Navbar></Navbar>
         <SidebarContainer></SidebarContainer>
         <div className="content-wrapper">
            <ContentHeader
               titulo={"Contáctenos"}
               breadCrumb1={"Inicio"}
               breadCrumb2={"Contáctenos"}
               ruta1={"/home"}
            />
            <div className="content-wrapper" style={{ minHeight: 1416 }}>
               <section className="content">
                  <div className="card">
                     <div className="card-body row">
                        <div className="col-5 text-center d-flex align-items-center justify-content-center">
                           <div className="true">
                              <h2>Delta<strong>Pets</strong></h2>
                              <p className="lead mb-5">Campus Central UIS
                                 Carrera 27 Calle 9
                                 Bucaramanga<br />
                                 PBX: (+57) (7) 634 4000
                              </p>
                           </div>
                        </div>
                        <div className="col-7">
                           <div className="form-group">
                              <label htmlFor="inputName">Nombre</label>
                              <input type="text" id="inputName" className="form-control" />
                           </div>
                           <div className="form-group">
                              <label htmlFor="inputEmail">Correo Electrónico</label>
                              <input type="email" id="inputEmail" className="form-control" />
                           </div>
                           <div className="form-group">
                              <label htmlFor="inputSubject">Asunto</label>
                              <input type="text" id="inputSubject" className="form-control" />
                           </div>
                           <div className="form-group">
                              <label htmlFor="inputMessage">Mensaje</label>
                              <textarea id="inputMessage" className="form-control" rows={4} defaultValue={""} />
                           </div>
                           <div className="form-group">
                              <input type="submit" className="btn btn-primary" defaultValue="Send message" />
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
            </div>
         </div>
         <Footer></Footer>
      </div>




   );
}

export default Contactenos;