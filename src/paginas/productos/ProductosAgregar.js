import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
//import { Link } from 'react-router-dom';
import swal from 'sweetalert';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import APIInvoke from '../../utils/APIInvoke';
import { useNavigate } from 'react-router-dom';

const ProductosAgregar = () => {

   //CONSUMIR LOS DATOS
   const navigate = useNavigate();

   const [producto, setProducto] = useState({
      nombre: '',
      categoriaMascota: '',
      categoriaProducto: '',
      precio: ''
   });

   const { nombre, categoriaMascota, categoriaProducto, precio } = producto;

   const onChange = (e) => {
      setProducto({
         ...producto,
         [e.target.name]: e.target.value
      })
      console.log(producto);
   }



   // El foco del puntero
   useEffect(() => {
      document.getElementById("nombre").focus();
   }, [])

   const [productos, setProductos] = useState([]);

   const cargarProductos = async () => {
      const response = await APIInvoke.invokeGET(`/api/productos`);
      console.log(response.productos);
      setProductos(response.productos);
   }

   useEffect(() => {
      cargarProductos();
   }, [])

   const eliminarProducto = async (e, idProducto) => {
      e.preventDefault();
      const response = await APIInvoke.invokeDELETE(`/api/productos/${idProducto}`);

      if (response.msg === 'Producto eliminado') {
         const msg = "El producto fue borrado correctamente.";
         swal({
            title: 'Información',
            text: msg,
            icon: 'success',
            buttons: {
               confirm: {
                  text: 'Ok',
                  value: true,
                  visible: true,
                  className: 'btn btn-primary',
                  closeModal: true
               }
            }
         });
         cargarProductos();
      } else {
         const msg = "El producto no fue borrado correctamente.";
         swal({
            title: 'Error',
            text: msg,
            icon: 'error',
            buttons: {
               confirm: {
                  text: 'Ok',
                  value: true,
                  visible: true,
                  className: 'btn btn-danger',
                  closeModal: true
               }
            }
         });
      }
   }

   const agregarProducto = async () => {
      const data = {
         nombre: producto.nombre,
         categoriaMascota: producto.categoriaMascota,
         categoriaProducto: producto.categoriaProducto,
         precio: producto.precio
      }
      const response = await APIInvoke.invokePOST(`/api/productos`, data);
      const idProducto = response._id;

      if (idProducto === '') {
         const msg = "El producto no fue agregado correctamente.";
         swal({
            title: 'Error',
            text: msg,
            icon: 'error',
            buttons: {
               confirm: {
                  text: 'Ok',
                  value: true,
                  visible: true,
                  className: 'btn btn-danger',
                  closeModal: true
               }
            }
         });
      } else {
         navigate("/productos-admin");
         const msg = "El producto fue agregado correctamente.";
         swal({
            title: 'Información',
            text: msg,
            icon: 'success',
            buttons: {
               confirm: {
                  text: 'Ok',
                  value: true,
                  visible: true,
                  className: 'btn btn-primary',
                  closeModal: true
               }
            }
         });

         setProducto({
            nombre: ''
         })
      }
   }

   const onSubmit = (e) => {
      e.preventDefault();
      agregarProducto();
   }

   return (

      <div className="wrapper">
         <Navbar></Navbar>
         <SidebarContainer></SidebarContainer>
         <div className="content-wrapper">

            <ContentHeader
               titulo={"Agregar Productos"}
               breadCrumb1={"Productos (Admin)"}
               breadCrumb2={"DAgregar (Admin)"}
               ruta1={"/productos-admin"}
            />

            <section className="content">
               <div className="card">

                  {/*AGREGAR PRODUCTO */}
                  <div className="card card-primary">
                     <form onSubmit={onSubmit}>
                        <div className="card-body">

                           <div className="form-group">
                              <label htmlFor="exampleInputEmail1">Nombre</label>
                              <input type="text"
                                 className="form-control"
                                 id="nombre"
                                 name="nombre"
                                 placeholder="Digite el nombre del Producto"
                                 value={nombre}
                                 onChange={onChange}
                                 required
                              />
                           </div>


                           <div className="form-group">
                              <label htmlFor="status">Categoría de Mascota</label>
                              <select id="categoriaMascota" name="categoriaMascota"
                                 className="form-control custom-select"
                                 value={categoriaMascota}
                                 onChange={onChange}
                                 required >
                                 <option value={""} defaultValue disabled>Seleccione una categoría</option>
                                 <option value={"Perros"}> Perros</option>
                                 <option value={"Gatos"}> Gatos</option>
                                 <option value={"Aves"}> Aves</option>
                                 <option value={"Hamsters"}> Hamsters</option>
                              </select>
                           </div>


                           {/*  <div className="form-group">
                              <label htmlFor="exampleInputEmail1">Categoría de Mascota</label>
                              <input type="text"
                                 className="form-control"
                                 id="categoriaMascota"
                                 name="categoriaMascota"
                                 placeholder="Digite Perros, Gatos, Aves o Hamster"
                                 value={categoriaMascota}
                                 onChange={onChange}
                                 required
                              />
                           </div>
   */}

                           <div className="form-group">
                              <label htmlFor="status">Categoría de Producto</label>
                              <select id="categoriaProducto" name="categoriaProducto"
                                 className="form-control custom-select"
                                 value={categoriaProducto}
                                 onChange={onChange}
                                 required >
                                 <option value={""} defaultValue disabled>Seleccione una categoría</option>
                                 <option value={"Comida"}> Comida</option>
                                 <option value={"Aseo"}> Aseo</option>
                                 <option value={"Juguetes"}> Juguetes</option>
                                 <option value={"Accesorios"}> Accesorios</option>
                              </select>
                           </div>
{/*
                           <div className="form-group">
                              <label htmlFor="exampleInputEmail1">Categoría de Producto</label>
                              <input type="text"
                                 className="form-control"
                                 id="categoriaProducto"
                                 name="categoriaProducto"
                                 placeholder="Digite Comida, Aseo, Juguetes o Accesorios"
                                 value={categoriaProducto}
                                 onChange={onChange}
                                 required
                              />
                           </div>
*/}
                           {/* PARA PROBAR CON MENU DESPLEGABLE}
                           <div className="form-group">
                              <label>Escoja la categoría de la mascota</label>
                              <select className="form-control">
                                 <option>Perros</option>
                                 <option>Gatos</option>
                                 <option>Aves</option>
                                 <option>Hamsters</option>
                              </select>
                           </div>
                           <div className="form-group">
                              <label>Escoja la categoría del producto</label>
                              <select className="form-control">
                                 <option>Comida</option>
                                 <option>Aseo</option>
                                 <option>Juguetes</option>
                                 <option>Accesorios</option>
                              </select>
                           </div>
*/}

                           <div className="form-group">
                              <label htmlFor="exampleInputEmail1">Precio</label>
                              <input type="number"
                                 className="form-control"
                                 id="precio"
                                 name="precio"
                                 placeholder="Digite el precio sin puntos ni comas"
                                 value={precio}
                                 onChange={onChange}
                                 required
                              />
                           </div>
                        </div>
                        <div className="card-footer">
                           <button type="submit" className="btn btn-primary">Agregar</button>
                        </div>
                     </form>
                  </div>
                  <div className="col-sm-12 col-md-6">

                     {/*Tabla de datos */}
                  </div>
                  <div className="card-body">
                     <div
                        id="example1_wrapper"
                        className="dataTables_wrapper dt-bootstrap4"
                     >
                        <div className="row">
                           <div className="col-sm-12">
                              <table
                                 id="example1"
                                 className="table table-bordered table-striped dataTable dtr-inline"
                                 aria-describedby="example1_info"
                              >
                                 <thead>
                                    <tr>
                                       <th
                                          style={{ width: '10%' }}
                                          className="sorting sorting_asc"
                                          tabIndex={0}
                                          aria-controls="example1"
                                          rowSpan={1}
                                          colSpan={1}
                                          aria-sort="ascending"
                                          aria-label="Rendering engine: activate to sort column descending"
                                       >
                                          Id
                                       </th>

                                       <th
                                          style={{ width: '30%' }}
                                          className="sorting sorting_asc"
                                          tabIndex={0}
                                          aria-controls="example1"
                                          rowSpan={1}
                                          colSpan={1}
                                          aria-sort="ascending"
                                          aria-label="Rendering engine: activate to sort column descending"
                                       >
                                          Nombre
                                       </th>

                                       <th
                                          style={{ width: '20%' }}
                                          className="sorting"
                                          tabIndex={0}
                                          aria-controls="example1"
                                          rowSpan={1}
                                          colSpan={1}
                                          aria-label="Browser: activate to sort column ascending"
                                       >
                                          Mascota
                                       </th>

                                       <th
                                          style={{ width: '10%' }}
                                          className="sorting"
                                          tabIndex={0}
                                          aria-controls="example1"
                                          rowSpan={1}
                                          colSpan={1}
                                          aria-label="Platform(s): activate to sort column ascending"
                                       >
                                          Categoría
                                       </th>

                                       <th
                                          style={{ width: '10%' }}
                                          className="sorting"
                                          tabIndex={0}
                                          aria-controls="example1"
                                          rowSpan={1}
                                          colSpan={1}
                                          aria-label="Engine version: activate to sort column ascending"
                                       >
                                          Precio
                                       </th>

                                       <th
                                          style={{ width: '20%' }}
                                          className="sorting"
                                          tabIndex={0}
                                          aria-controls="example1"
                                          rowSpan={1}
                                          colSpan={1}
                                          aria-label="Engine version: activate to sort column ascending"
                                       >
                                          Opciones
                                       </th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    {
                                       productos.map(
                                          item =>
                                             <tr key={item._id}>
                                                <td>{item._id}</td>
                                                <td>{item.nombre}</td>
                                                <td>{item.categoriaMascota}</td>
                                                <td>{item.categoriaProducto}</td>
                                                <td>{item.precio}</td>
                                                <td>
                                                   <button className="btn btn-sm btn-primary">
                                                      <i className="fas fa-pencil-alt"> </i> Editar</button>

                                                   <button onClick={(e) => eliminarProducto(e, item._id)}
                                                      className="btn btn-sm btn-danger">
                                                      <i className="far fa-trash-alt"> </i> Borrar</button>

                                                </td>
                                             </tr>
                                       )
                                    }
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
         </div>
         <Footer></Footer>
      </div>
   );
}

export default ProductosAgregar;