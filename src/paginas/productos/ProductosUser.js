import React, { useState, useEffect } from "react";
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
//import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
//import swal from 'sweetalert';


const ProductosUser = () => {

    const [productos, setProductos] = useState([]);
    const [search, setSearch] = useState("")

    const cargarProductos = async () => {
        const response = await APIInvoke.invokeGET(`/api/productos`);
        console.log(response.productos);
        setProductos(response.productos);
    }

    useEffect(() => {
        cargarProductos();
    }, [])

    const searchEvent = async () => {

        const response = await APIInvoke.invokeGET(`/api/productos/list/${search}`);
        setProductos(response);

    };
    const onChange = (e) => {
        setSearch(e.target.value);
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">

                <ContentHeader
                    titulo={"Listado de Productos"}
                    breadCrumb1={"Inicio"}
                    breadCrumb2={"Productos"}
                    ruta1={"/home"}
                />

                <section className="content">
                    <div className="card">
                        

                    <div className="col-sm-4 col-md-5"> <br></br>
                            <div className="input-group" data-widget="sidebar-search">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input
                                    type="text"
                                    id="search"
                                    value={search}
                                    onChange={onChange}
                                    name="search"
                                    placeholder="Búsqueda"
                                    aria-label="Search" />
                                <div className="input-group-append">
                                    <button type="button" className="btn btn-primary"
                                        onClick={searchEvent} >
                                     <i className="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>



                        <div className="card-body">
                            <div
                                id="example1_wrapper"
                                className="dataTables_wrapper dt-bootstrap4"
                            >
                                <div className="row">
                                    <div className="col-sm-12">
                                        <table
                                            id="example1"
                                            className="table table-bordered table-striped dataTable dtr-inline"
                                            aria-describedby="example1_info"
                                        >
                                            <thead>
                                                <tr>
                                                    <th
                                                        style={{ width: '40%' }}
                                                        className="sorting sorting_asc"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending"
                                                    >
                                                        Nombre
                                                    </th>

                                                    <th
                                                        style={{ width: '18%' }}
                                                        className="sorting"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-label="Browser: activate to sort column ascending"
                                                    >
                                                        Mascota
                                                    </th>

                                                    <th
                                                        style={{ width: '18%' }}
                                                        className="sorting"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-label="Platform(s): activate to sort column ascending"
                                                    >
                                                        Categoría
                                                    </th>

                                                    <th
                                                        style={{ width: '18%' }}
                                                        className="sorting"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-label="Engine version: activate to sort column ascending"
                                                    >
                                                        Precio
                                                    </th>

                                                    <th
                                                        style={{ width: '6%' }}
                                                        className="sorting"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-label="Engine version: activate to sort column ascending"
                                                    >
                                                        Carrito
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    productos.map(
                                                        item =>
                                                            <tr key={item._id}>
                                                                {/* <td>{item._id}</td>*/}
                                                                <td>{item.nombre}</td>
                                                                <td>{item.categoriaMascota}</td>
                                                                <td>{item.categoriaProducto}</td>
                                                                <td>{item.precio}</td>
                                                                <td>
                                                                    &nbsp;
                                                                    <button className="btn btn-sm btn-warning">  &nbsp;
                                                                        <i className="fas fa-cart-plus">&nbsp;  </i> </button>


                                                                </td>

                                                            </tr>
                                                    )
                                                }

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
            <Footer></Footer>
        </div>
    );
}

export default ProductosUser;