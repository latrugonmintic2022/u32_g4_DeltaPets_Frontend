import React, { useEffect, useState } from 'react';
import swal from 'sweetalert';
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import APIInvoke from '../../utils/APIInvoke';
import { useNavigate, useParams } from 'react-router-dom';

const ProductosEditar = () => {

    const navigate = useNavigate();
    const { idproducto } = useParams();
    let arreglo = idproducto.split('@');

    const nombreProducto = arreglo[1];
    const catMascota = arreglo[2];
    const catProducto = arreglo[3];
    const precioo = arreglo[4];

    const tituloPagina = `Editar Producto: ${nombreProducto}`;

    const [producto, setProductos] = useState({
        nombre: nombreProducto,
        categoriaMascota: catMascota,
        categoriaProducto: catProducto,
        precio: precioo
    });

    const { nombre, categoriaMascota, categoriaProducto, precio } = producto;

    useEffect(() => {
        document.getElementById("nombre").focus();
    }, [])

    const onChange = (e) => {
        setProductos({
            ...producto,
            [e.target.name]: e.target.value
        })
        console.log(producto);
    }

    const editarProducto = async () => {
        let arreglo = idproducto.split('@');
        const idProducto = arreglo[0];

        const data = {
            //  producto: idProducto,
            nombre: producto.nombre,
            categoriaMascota: producto.categoriaMascota,
            categoriaProducto: producto.categoriaProducto,
            precio: producto.precio,
        }

        const response = await APIInvoke.invokePUT(`/api/productos/${idProducto}`, data);
        const idProductoEditado = response.producto._id;

        if (idProductoEditado !== idProducto) {
            const msg = "El producto no fue editado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        } else {
            navigate(`/productos-admin/`);
            const msg = "Producto editado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
        }
    }

    const onSubmit = (e) => {
        e.preventDefault();
        editarProducto();
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">
                <ContentHeader
                    titulo={tituloPagina}
                    breadCrumb1={"Productos (Admin)"}
                    breadCrumb2={"Editar (Admin)"}
                    ruta1={"/productos-admin"}
                />
                <section className="content">
                    <div className="card card-primary">
                        <form onSubmit={onSubmit}>
                            <div className="card-body">
                                <div className="form-group">
                                    <label htmlFor="nombre">Nombre</label>
                                    <input type="text"
                                        className="form-control"
                                        id="nombre"
                                        name="nombre"
                                        placeholder="Digite el nombre del Producto"
                                        value={nombre}
                                        onChange={onChange}
                                        required
                                    />
                                </div>

                       <div className="form-group">
                              <label htmlFor="status">Categoría de Mascota</label>
                              <select id="categoriaMascota" name="categoriaMascota"
                                 className="form-control custom-select"
                                 value={categoriaMascota}
                                 onChange={onChange}
                                 required >
                                 <option value={""} defaultValue disabled>Seleccione una categoría</option>
                                 <option value={"Perros"}> Perros</option>
                                 <option value={"Gatos"}> Gatos</option>
                                 <option value={"Aves"}> Aves</option>
                                 <option value={"Hamsters"}> Hamsters</option>
                              </select>
                           </div>

                                <div className="form-group">
                              <label htmlFor="status">Categoría de Producto</label>
                              <select id="categoriaProducto" name="categoriaProducto"
                                 className="form-control custom-select"
                                 value={categoriaProducto}
                                 onChange={onChange}
                                 required >
                                 <option value={""} defaultValue disabled>Seleccione una categoría</option>
                                 <option value={"Comida"}> Comida</option>
                                 <option value={"Aseo"}> Aseo</option>
                                 <option value={"Juguetes"}> Juguetes</option>
                                 <option value={"Accesorios"}> Accesorios</option>
                              </select>
                           </div>
                                <div className="form-group">
                                    <label htmlFor="precio">Precio</label>
                                    <input type="number"
                                        className="form-control"
                                        id="precio"
                                        name="precio"
                                        placeholder="Digite el precio sin puntos ni comas"
                                        value={precio}
                                        onChange={onChange}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="card-footer">
                                <button type="submit" className="btn btn-primary">Editar</button>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default ProductosEditar;