import React, { useState, useEffect } from "react";
import ContentHeader from '../../componentes/ContentHeader';
import Footer from '../../componentes/Footer';
import Navbar from '../../componentes/Navbar';
import SidebarContainer from '../../componentes/SidebarContainer';
import { Link } from 'react-router-dom';
import APIInvoke from '../../utils/APIInvoke'
import swal from 'sweetalert';

const ProductosAdmin = () => {

    const [productos, setProductos] = useState([]);
    const [search, setSearch] = useState("")

    const cargarProductos = async () => {
        const response = await APIInvoke.invokeGET(`/api/productos`);
        console.log(response.productos);
        setProductos(response.productos);
    }


    useEffect(() => {
        cargarProductos();
    }, [])

    const eliminarProducto = async (e, idProducto) => {
        e.preventDefault();
        const response = await APIInvoke.invokeDELETE(`/api/productos/${idProducto}`);

        if (response.msg === 'Producto eliminado') {
            const msg = "El producto fue borrado correctamente.";
            swal({
                title: 'Información',
                text: msg,
                icon: 'success',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-primary',
                        closeModal: true
                    }
                }
            });
            cargarProductos();
        } else {
            const msg = "El producto no fue borrado correctamente.";
            swal({
                title: 'Error',
                text: msg,
                icon: 'error',
                buttons: {
                    confirm: {
                        text: 'Ok',
                        value: true,
                        visible: true,
                        className: 'btn btn-danger',
                        closeModal: true
                    }
                }
            });
        }
    }

    const searchEvent = async () => {

        const response = await APIInvoke.invokeGET(`/api/productos/list/${search}`);
        setProductos(response);

    };
    const onChange = (e) => {
        setSearch(e.target.value);
    }

    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">
                <ContentHeader
                    titulo={"Listado de Productos"}
                    breadCrumb1={"Productos (Admin)"}
                    breadCrumb2={"Deltapets"}
                    ruta1={"/productos-admin"}
                />
                <section className="content">
                    <div className="card">


                        <div className="col-sm-4 col-md-5"> <br></br>
                            <div className="input-group" data-widget="sidebar-search">
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <input
                                    type="text"
                                    id="search"
                                    value={search}
                                    onChange={onChange}
                                    name="search"
                                    placeholder="Búsqueda"
                                    aria-label="Search" />
                                <div className="input-group-append">
                                    <button type="button"  className="btn btn-primary"
                                        onClick={searchEvent} >
                                       <i className="fas fa-search"></i>
                                    </button>
                                    &nbsp;&nbsp;&nbsp;&nbsp;

                                </div>
                                <li className="nav-item d-none d-sm-inline-block">
                                    <Link to={"/productos-agregar"} className="btn btn-md btn-primary">
                                        Agregar Producto</Link>
                                </li>
                            </div>
                        </div>

                        <div className="card-body">
                            <div
                                id="example1_wrapper"
                                className="dataTables_wrapper dt-bootstrap4"
                            >
                                <div className="row">
                                    <div className="col-sm-12">
                                        <table
                                            id="example1"
                                            className="table table-bordered table-striped dataTable dtr-inline"
                                            aria-describedby="example1_info"
                                        >
                                            <thead>
                                                <tr>
                                                    <th
                                                        style={{ width: '5%' }}
                                                        className="sorting sorting_asc"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending"
                                                    >
                                                        Id
                                                    </th>
                                                    <th
                                                        style={{ width: '40%' }}
                                                        className="sorting sorting_asc"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-sort="ascending"
                                                        aria-label="Rendering engine: activate to sort column descending"
                                                    >
                                                        Nombre
                                                    </th>
                                                    <th
                                                        style={{ width: '10%' }}
                                                        className="sorting sorting_asc"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-sort="ascending"
                                                        aria-label="Browser: activate to sort column ascending"
                                                    >
                                                        Mascota
                                                    </th>
                                                    <th
                                                        style={{ width: '10%' }}
                                                        className="sorting"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-label="Platform(s): activate to sort column ascending"
                                                    >
                                                        Categoría
                                                    </th>
                                                    <th
                                                        style={{ width: '10%' }}
                                                        className="sorting sorting_asc"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-sort="ascending"
                                                        aria-label="Engine version: activate to sort column ascending"
                                                    >
                                                        Precio
                                                    </th>
                                                    <th
                                                        style={{ width: '25%' }}
                                                        className="sorting sorting_asc"
                                                        tabIndex={0}
                                                        aria-controls="example1"
                                                        rowSpan={1}
                                                        colSpan={1}
                                                        aria-sort="ascending"
                                                        aria-label="Engine version: activate to sort column ascending"
                                                    >
                                                        Opciones
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {productos.map(
                                                    item =>
                                                        <tr key={item._id}>
                                                            <td>{item._id}</td>
                                                            <td>{item.nombre}</td>
                                                            <td>{item.categoriaMascota}</td>
                                                            <td>{item.categoriaProducto}</td>
                                                            <td>{item.precio}</td>
                                                            <td>
                                                                <Link to={`/productos-editar/${item._id}@${item.nombre}@${item.categoriaMascota}@${item.categoriaProducto}@${item.precio}`} className="btn btn-sm btn-primary">
                                                                    <i className="fas fa-pencil-alt"> </i> Editar</Link>
                                                                &nbsp; &nbsp; &nbsp;
                                                                <button onClick={(e) => eliminarProducto(e, item._id)}
                                                                    className="btn btn-sm btn-danger">
                                                                    <i className="far fa-trash-alt"> </i> Borrar</button>
                                                            </td>
                                                        </tr>
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                {/*
                                <div className="row">
                                    <div className="col-sm-12 col-md-5">
                                        <div
                                            className="dataTables_info"
                                            id="example1_info"
                                            role="status"
                                            aria-live="polite"
                                        >
                                            Showing 1 to 10 of 57 entries
                                        </div>
                                    </div>
                                    <div className="col-sm-12 col-md-7">
                                        <div
                                            className="dataTables_paginate paging_simple_numbers"
                                            id="example1_paginate"
                                        >
                                            <ul className="pagination">
                                                <li
                                                    className="paginate_button page-item previous disabled"
                                                    id="example1_previous"
                                                >
                                                    <Link
                                                        to={"#"}
                                                        aria-controls="example1"
                                                        data-dt-idx={0}
                                                        tabIndex={0}
                                                        className="page-link"
                                                    >
                                                        Anterior
                                                    </Link>
                                                </li>
                                                <li className="paginate_button page-item active">
                                                    <Link
                                                        to={"#"}
                                                        aria-controls="example1"
                                                        data-dt-idx={1}
                                                        tabIndex={0}
                                                        className="page-link"
                                                    >
                                                        1
                                                    </Link>
                                                </li>
                                                <li className="paginate_button page-item ">
                                                    <Link
                                                        to={"#"}
                                                        aria-controls="example1"
                                                        data-dt-idx={2}
                                                        tabIndex={0}
                                                        className="page-link"
                                                    >
                                                        2
                                                    </Link>
                                                </li>
                                                <li className="paginate_button page-item ">
                                                    <Link
                                                        to={"#"}
                                                        aria-controls="example1"
                                                        data-dt-idx={3}
                                                        tabIndex={0}
                                                        className="page-link"
                                                    >
                                                        3
                                                    </Link>
                                                </li>
                                                <li className="paginate_button page-item ">
                                                    <Link
                                                        to={"#"}
                                                        aria-controls="example1"
                                                        data-dt-idx={4}
                                                        tabIndex={0}
                                                        className="page-link"
                                                    >
                                                        4
                                                    </Link>
                                                </li>
                                                <li className="paginate_button page-item ">
                                                    <Link
                                                        to={"#"}
                                                        aria-controls="example1"
                                                        data-dt-idx={5}
                                                        tabIndex={0}
                                                        className="page-link"
                                                    >
                                                        5
                                                    </Link>
                                                </li>
                                                <li className="paginate_button page-item ">
                                                    <Link
                                                        to={"#"}
                                                        aria-controls="example1"
                                                        data-dt-idx={6}
                                                        tabIndex={0}
                                                        className="page-link"
                                                    >
                                                        6
                                                    </Link>
                                                </li>
                                                <li
                                                    className="paginate_button page-item next"
                                                    id="example1_next"
                                                >
                                                    <Link
                                                        to={"#"}
                                                        aria-controls="example1"
                                                        data-dt-idx={7}
                                                        tabIndex={0}
                                                        className="page-link"
                                                    >
                                                        Siguiente
                                                    </Link>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                */}
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default ProductosAdmin;