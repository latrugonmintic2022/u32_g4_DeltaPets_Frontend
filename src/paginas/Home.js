import React from 'react';
import ContentHeader from '../componentes/ContentHeader';
import Footer from '../componentes/Footer';
import Navbar from '../componentes/Navbar';
import SidebarContainer from '../componentes/SidebarContainer';
import Categorias from '../componentes/Categorias';
import Overlay from '../componentes/Overlay';

const Home = () => {
    return (
        <div className="wrapper">
            <Navbar></Navbar>
            <SidebarContainer></SidebarContainer>
            <div className="content-wrapper">
                
                <ContentHeader
                 
                titulo={"Deltapets"}
                breadCrumb1={"Inicio"}
                breadCrumb2={"Deltapets"}
                ruta1={"/home"}
                
                />
            
                <Categorias></Categorias>
                <Overlay></Overlay>
            </div>
            <Footer></Footer>
        </div>
    );
}

export default Home;