import React, {Fragment} from 'react';
import { BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import Categorias from './componentes/Categorias';
import CrearCuenta from './paginas/auth/CrearCuenta';
import Login from './paginas/auth/Login';
import Carrito from './paginas/contactenos/carrito';
import Contactenos from './paginas/contactenos/contactenos';
import Home from './paginas/Home';
import ProductosAdmin from './paginas/productos/ProductosAdmin';
import ProductosAgregar from './paginas/productos/ProductosAgregar';
import ProductosEditar from './paginas/productos/ProductosEditar';
import ProductosUser from './paginas/productos/ProductosUser';





function App() {
  return (
    <div> 
      <Fragment>
      <Router>
        <Routes>
          <Route path="/" exact element={<Login/>}/>
          <Route path="/crear-cuenta" exact element={<CrearCuenta/>}/>
          <Route path="/home" exact element={<Home/>}/>
          <Route path="/productos-admin" exact element={<ProductosAdmin/>}/>
          <Route path="/productos-user" exact element={<ProductosUser/>}/>
          <Route path="/productos-agregar" exact element={<ProductosAgregar/>}/>
          <Route path="/productos-editar/:idproducto" exact element={<ProductosEditar/>}/>

          <Route path="/contactenos" exact element={<Contactenos/>}/>
          <Route path="/categorias" exact element={<Categorias/>}/>
          <Route path="/carrito" exact element={<Carrito/>}/>


       
        </Routes>
      </Router>
    </Fragment>
    </div>

    
  );
}

export default App;
